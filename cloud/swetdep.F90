!> \file
!> \brief Wet deposition of trace gases or aerosols
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine swetdep(dpxdt,pdclr,pdcld,dwr1,dwr2,dwr3, &
                         dws1,dws2,dws3,pxclr,pxcld,phenry, &
                         pcef,rhoa,dpa,pmrep,zmlwc,pfsnow,pfrain, &
                         zclf,clrfr,clrfs,pfevap,pfsubl,zcthr,dt, &
                         ilga,leva,isec)
  !
  use sdphys, only : grav
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isec !< Number of separate aerosol tracers
  real, intent(in) :: dt !< Time step \f$[s]\f$
  real, intent(in), dimension (ilga,leva,isec) :: pxclr !< The total aerosol concentration in the clear part of the grid cell \f$[kg/kg]\f$ or \f$[1/kg]\f$
  real, intent(in), dimension (ilga,leva,isec) :: pxcld !< The total aerosol concentration in the cloudy part of the grid cell \f$[kg/kg]\f$ or \f$[1/kg]\f$
  real, intent(in), dimension (ilga,leva,isec) :: phenry !< The fraction of aerosol that is activated
  real, intent(in), dimension (ilga,leva,isec) :: pcef !< Collection efficiency
  real, intent(in), dimension (ilga,leva) :: zmlwc !< Liquid water content in the cloudy part of the grid cell \f$[kg/kg]\f$
  real, intent(in), dimension (ilga,leva) :: rhoa !< Air density \f$[kg/m^3]\f$
  real, intent(in), dimension (ilga,leva) :: dpa !< Difference in air pressure between bottom and top of grid cell \f$[Pa]\f$
  real, intent(in), dimension (ilga,leva) :: pmrep !< Change in cloud liquid water content due to autoconversion and accretion in a model time step \f$[kg/kg/step]\f$
  !< i.e. the sum of all cloud microphysical processes that convert cloud water to rain.
  !< Note that PMREP is multiplied by 2 at the beginning of PAM if the time step in PAM is doubled
  real, intent(in), dimension (ilga,leva) :: pfsnow !< Grid-cell mean snowfall rate \f$[kg/m2/sec]\f$, at bottom of each grid cell
  real, intent(in), dimension (ilga,leva) :: pfevap !< Fraction of rain that completely evaporates during the model time step in the clear air
  real, intent(in), dimension (ilga,leva) :: zclf !< Cloud fraction
  real, intent(in), dimension (ilga,leva) :: clrfr !< Rain fraction (i.e. clear-sky part of grid cell that contains rain)
  real, intent(in), dimension (ilga,leva) :: pfrain !< Grid-cell mean rainfall rate \f$[kg/m2/sec]\f$, at bottom of each grid cell
  real, intent(in), dimension (ilga,leva) :: clrfs !< Snow fraction (i.e. clear-sky part of grid cell that contains snow)
  real, intent(in), dimension (ilga,leva) :: pfsubl !< Fraction of snow that sublimates in a model time step
  real, intent(out), dimension (ilga,leva,isec) :: dpxdt !< Wet removal tendency
  real, intent(out), dimension (ilga,leva,isec) :: pdclr !<
  real, intent(out), dimension (ilga,leva,isec) :: pdcld !<
  real, intent(out), dimension (ilga,leva,isec) :: dwr1 !< The rate of change in total aerosol concentration due to in-cloud scavenging in water clouds \f$[kg/kg/s]\f$
  real, intent(out), dimension (ilga,leva,isec) :: dwr2 !< The rate of change in total aerosol concentration due to below-cloud scavenging for water clouds \f$[kg/kg/s]\f$
  real, intent(out), dimension (ilga,leva,isec) :: dwr3 !< The rate of change in total aerosol concentration due to rain evaporation \f$[kg/kg/s]\f$
  real, intent(out), dimension (ilga,leva,isec) :: dws1 !< The rate of change in total aerosol concentration due to in-cloud scavenging in ice clouds \f$[kg/kg/s]\f$
  real, intent(out), dimension (ilga,leva,isec) :: dws2 !< The rate of change in total aerosol concentration due to below-cloud scavenging for ice clouds \f$[kg/kg/s]\f$
  real, intent(out), dimension (ilga,leva,isec) :: dws3 !< The rate of change in total aerosol concentration due to snow sublimation \f$[kg/kg/s]\f$
  real, dimension(ilga,isec) :: zdepr !< the aerosol wet deposition flux associated with liquid precipitation (rain)
  real, dimension(ilga,isec) :: zdeps !< the aerosol wet deposition flux associated with solid precipitation (snow)
  real, dimension(ilga,leva,isec) :: pdepr !<
  real, dimension(ilga,leva,isec) :: pdeps !<
  !
  !     internal work variables
  !
  integer :: il !<
  integer :: is !<
  integer :: l !<
  real :: zftom !<
  real :: zbcscav !< The fraction of the total aerosol concentration that is removed by below-cloud scavenging in one time step
  real :: zcthr !<
  real :: zmtof !<
  real :: zicscav !< The fraction of the total aerosol concentration that is removed by in-cloud scavenging in one time step
  real :: zeps !< Fraction of the aerosol concentration in the cloud droplets, i.e. the fraction of activated aerosol
  real, parameter :: zero=0. !<
  real, parameter :: one =1. !<
  real, parameter :: zmin=1.e-20 !<
  real, parameter :: zclrfm=0.05 !<
  real, parameter :: zep=1. !<
  !
  !-----------------------------------------------------------------------
  !     * initialization.
  !
  pdeps=0.
  pdepr=0.
  zdepr=0.
  zdeps=0.
  dpxdt=0.
  pdcld=0.
  pdclr=0.
  dwr1=0.
  dwr2=0.
  dwr3=0.
  dws1=0.
  dws2=0.
  dws3=0.
  !
  !-----------------------------------------------------------------------
  !
  do is=1,isec
    do l=1,leva
      !
      !     * in-cloud scavenging for liquid water clouds.
      !
      !     if pmrep (the per time step change in cloud liquid water content due to autoconversion and accretion) is greater than zmin,
      !     and if zmlwc (the liquid water content in the cloudy portion of the grid cell) also exceeds zmin, then wet deposition is calculated.
      !
      !     the fraction of aerosol in the cloud droplets that is scavenged is calculated as the ratio of
      !     autoconversion + accretion (pmrep) over the liquid water content (zmlwc), i.e. zicscav=pmrep(il,l)/zmlwc(il,l).
      !
      !     the fraction of the aerosol concentration in the cloud droplets, i.e. the fraction of activated aerosol, is calculated
      !     as zeps=zep*phenry(il,l,is), where phenry is the fraction of the aerosol that is activated and zep=1. to get phenry, the concentration
      !     of activated aerosol is determined by integration of the aerosol size distribution in the cloudy part
      !     of the grid cell between the critical particle radius and the upper radius bound of the size distribution.
      !     this result is then divided by the total aerosol concentration.
      !
      !     the fraction of the total aerosol concentration that is removed by in-cloud scavenging in one time step is calculated
      !     as zicscav=zeps*zicscav
      !
      !     the change in total aerosol concentration is calculated by dwr1(il,l,is)=pxcld(il,l,is)*zicscav*zclf(il,l), where pxcld is the total
      !     aerosol concentration (in the cloudy part of the grid cell) and zclf is the cloud fraction (between 0 and 1). note that dwr1/dt is the
      !     rate of concentration change due to in-cloud scavenging for the entire grid cell (including non-cloudy air). this is calculated at the end
      !     of the subtroutine and saved in the model output.
      !
      do il=1,ilga
        if (pmrep(il,l) > zmin) then
          if (zmlwc(il,l) > zmin) then
            zicscav=pmrep(il,l)/zmlwc(il,l)
          else
            zicscav=zero
          end if
          zicscav=max(zero,min(one,zicscav))
          zeps=zep*phenry(il,l,is)
          zicscav=zeps*zicscav
          dwr1(il,l,is)=pxcld(il,l,is)*zicscav*zclf(il,l)
          pdepr(il,l,is)=pdepr(il,l,is)+dwr1(il,l,is)
          pdcld(il,l,is)=pdcld(il,l,is)+pxcld(il,l,is)*zicscav
        end if
      end do
      !
      !       * below cloud scavenging for ice and water clouds (scott, 1978).
      !
      do il=1,ilga
        if (pfsnow(il,l) > zmin .and. clrfs(il,l) >= zclrfm) then
          zftom=dt*grav/dpa(il,l)
          zbcscav=3.7*pcef(il,l,is)*(pfsnow(il,l)/clrfs(il,l))*zftom &
                   *rhoa(il,l)
          zbcscav=max(zero,min(one,zbcscav))
          dws2(il,l,is)=zbcscav*pxclr(il,l,is)*clrfs(il,l)
          pdeps(il,l,is)=pdeps(il,l,is)+dws2(il,l,is)
          if (zclf(il,l) < zcthr) then
            pdclr(il,l,is)=pdclr(il,l,is)+zbcscav*pxclr(il,l,is) &
                            *clrfs(il,l)/(1.-zclf(il,l))
          end if
        end if
      end do
      do il=1,ilga
        if (pfrain(il,l) > zmin .and. clrfr(il,l) >= zclrfm) then
          zftom=dt*grav/dpa(il,l)
          zbcscav=5.2*pcef(il,l,is)*(pfrain(il,l)/clrfr(il,l))*zftom &
                   *rhoa(il,l)
          zbcscav=max(zero,min(one,zbcscav))
          dwr2(il,l,is)=zbcscav*pxclr(il,l,is)*clrfr(il,l)
          pdepr(il,l,is)=pdepr(il,l,is)+dwr2(il,l,is)
          if (zclf(il,l) < zcthr) then
            pdclr(il,l,is)=pdclr(il,l,is)+zbcscav*pxclr(il,l,is) &
                            *clrfr(il,l)/(1.-zclf(il,l))
          end if
        end if
      end do
      do il=1,ilga
        zmtof=dpa(il,l)/(dt*grav)
        zdepr(il,is)=zdepr(il,is)+pdepr(il,l,is)*zmtof
        zdeps(il,is)=zdeps(il,is)+pdeps(il,l,is)*zmtof
      end do
      !
      !       * reevaporation. only complete evaporation of precip in the
      !       * clear-sky portion of the grid cell that is affected by rain
      !       * will cause tracers to be released into the environment
      !
      !       the change in total aerosol concentration is calculated as dwr3(il,l,is)=-zdepr(il,is)*zftom*pfevap(il,l),
      !       where zdepr is the preliminary 3d aerosol wet deposition flux associated with liquid precipitation (rain)
      !       before the evaporation of rain, and pfevap is the fraction of rain that completely evaporates
      !       during the model time step in the clear air. (note: pfevap = (pcp_t - pcp-b)/pcp_t, where pcp_t is the precipitation flux (kg/m2/sec)
      !       at the top of the grid cell and pcp_b the flux at the bottom, and the difference between them is due to evaporation).
      !       the 3d aerosol wet deposition flux (zdepr) is updated if pfevap > 0, i.e. if the rain droplets have completely evaporated.
      !
      do il=1,ilga
        zftom=dt*grav/dpa(il,l)
        dws3(il,l,is)=-zdeps(il,is)*zftom*pfsubl(il,l)
        pdeps(il,l,is)=pdeps(il,l,is)+dws3(il,l,is)
        zdeps(il,is)=zdeps(il,is)*(1.-pfsubl(il,l))
        if (zclf(il,l) < zcthr) then
          pdclr(il,l,is)=pdclr(il,l,is) &
                       -zdeps(il,is)*zftom*pfsubl(il,l)/(1.-zclf(il,l))
        end if
      end do
      do il=1,ilga
        zftom=dt*grav/dpa(il,l)
        dwr3(il,l,is)=-zdepr(il,is)*zftom*pfevap(il,l)
        pdepr(il,l,is)=pdepr(il,l,is)+dwr3(il,l,is)
        zdepr(il,is)=zdepr(il,is)*(1.-pfevap(il,l))
        if (zclf(il,l) < zcthr) then
          pdclr(il,l,is)=pdclr(il,l,is) &
                       -zdepr(il,is)*zftom*pfevap(il,l)/(1.-zclf(il,l))
        end if
      end do
      !
      !       * diagnose tendencies and concentrations.
      !       note: dwr[1-3]/dt or dws[1-3]/dt are the rates of concentration change
      !       for the entire grid cell including non-cloudy air
      !
      do il=1,ilga
        dpxdt(il,l,is)=-(pdepr(il,l,is)+pdeps(il,l,is))/dt
        dwr1(il,l,is)=-dwr1(il,l,is)/dt
        dwr2(il,l,is)=-dwr2(il,l,is)/dt
        dwr3(il,l,is)=-dwr3(il,l,is)/dt
        dws1(il,l,is)=-dws1(il,l,is)/dt
        dws2(il,l,is)=-dws2(il,l,is)/dt
        dws3(il,l,is)=-dws3(il,l,is)/dt
        !
        pdclr(il,l,is)=min(pdclr(il,l,is),pxclr(il,l,is))
        pdcld(il,l,is)=min(pdcld(il,l,is),pxcld(il,l,is))
      end do
    end do
  end do
  !
end subroutine swetdep
