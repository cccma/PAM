!> \file
!> \brief Diagnosis of vertically integrated sources and sinks of aerosol
!>       (OC, BC, sulphate, mineral dust, and sea salt) based on 3D
!>       tendencies.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine diagflx(voa,vbc,vsu,vmd,vss, &
                         pedmdt,pidmdt,pidfdt,pemas,pimas,pifrc, &
                         vitrm,dt,ilga,leva)
  !
  use sdparm, only : iexbc,iexmd,iexoc,iexso4,iexss, &
                         iinbc,iinmd,iinoc,iinso4,iinss, &
                         isaext,isaint, &
                         isextbc,isextmd,isextoc,isextso4,isextss, &
                         isintbc,isintmd,isintoc,isintso4,isintss, &
                         kint,kintbc,kintmd,kintoc,kintso4,kintss
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in) :: dt !< Model time step \f$[s]\f$
  real, intent(out), dimension(ilga) :: voa !< vertically integrated sources and sinks of OC aerosol
  real, intent(out), dimension(ilga) :: vbc !< vertically integrated sources and sinks of BC aerosol
  real, intent(out), dimension(ilga) :: vsu !< vertically integrated sources and sinks of sulfate aerosol
  real, intent(out), dimension(ilga) :: vmd !< vertically integrated sources and sinks of mineral dust aerosol
  real, intent(out), dimension(ilga) :: vss !< vertically integrated sources and sinks of sea salt aerosol
  real, intent(in), dimension(ilga,leva) :: vitrm !< Pressure in \f$kg/m^2\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pedmdt !< Mass tendency \f$[kg/kg/sec]\f$, ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pemas !< Aerosol (dry) mass concentration for externally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pidmdt !< Mass tendency \f$[kg/kg/sec]\f$, int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pidfdt !< Aerosol species mass fraction tendency \f$[1/sec]\f$, int mixture
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type
  real, dimension(ilga) :: tmp !<
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:,:) :: pemast !<
  real, allocatable, dimension(:,:,:) :: pimast !<
  real, allocatable, dimension(:,:,:,:) :: pifrct !<
  integer :: is !<
  integer :: isx !<
  integer :: l !<
  !
  !-----------------------------------------------------------------------
  !     * allocate work space.
  !
  if (isaext > 0) then
    allocate(pemast(ilga,leva,isaext))
  end if
  if (isaint > 0) then
    allocate(pimast(ilga,leva,isaint))
    allocate(pifrct(ilga,leva,isaint,kint))
  end if
  !
  !     * update aerosol number and mass.
  !
  if (isaext > 0) then
    pemast=max(pemas+dt*pedmdt,0.)
  end if
  if (isaint > 0) then
    pimast=max(pimas+dt*pidmdt,0.)
    pifrct=max(pifrc+dt*pidfdt,0.)
  end if
  voa=0.
  vbc=0.
  vsu=0.
  vmd=0.
  vss=0.
  !
  !     * diagnose column-integrated sources and sinks for externally
  !     * mixed types of aerosol.
  !
  if (isextoc > 0) then
    do is=1,isextoc
      isx=iexoc(is)
      do l=1,leva
        voa(:)=voa(:)+vitrm(:,l)*(pemast(:,l,isx)-pemas(:,l,isx))/dt
      end do
    end do
  end if
  if (isextbc > 0) then
    do is=1,isextbc
      isx=iexbc(is)
      do l=1,leva
        vbc(:)=vbc(:)+vitrm(:,l)*(pemast(:,l,isx)-pemas(:,l,isx))/dt
      end do
    end do
  end if
  if (isextso4 > 0) then
    do is=1,isextso4
      isx=iexso4(is)
      do l=1,leva
        vsu(:)=vsu(:)+vitrm(:,l)*(pemast(:,l,isx)-pemas(:,l,isx))/dt
      end do
    end do
  end if
  if (isextmd > 0) then
    do is=1,isextmd
      isx=iexmd(is)
      do l=1,leva
        vmd(:)=vmd(:)+vitrm(:,l)*(pemast(:,l,isx)-pemas(:,l,isx))/dt
      end do
    end do
  end if
  if (isextss > 0) then
    do is=1,isextss
      isx=iexss(is)
      do l=1,leva
        vss(:)=vss(:)+vitrm(:,l)*(pemast(:,l,isx)-pemas(:,l,isx))/dt
      end do
    end do
  end if
  !
  !     * the same as above for internally mixed aerosol.
  !
  if (isintoc > 0) then
    do is=1,isintoc
      isx=iinoc(is)
      do l=1,leva
        tmp=(pimast(:,l,isx)*pifrct(:,l,isx,kintoc) &
                -pimas (:,l,isx)*pifrc (:,l,isx,kintoc))/dt
        voa(:)=voa(:)+vitrm(:,l)*tmp(:)
      end do
    end do
  end if
  if (isintbc > 0) then
    do is=1,isintbc
      isx=iinbc(is)
      do l=1,leva
        tmp=(pimast(:,l,isx)*pifrct(:,l,isx,kintbc) &
                -pimas (:,l,isx)*pifrc (:,l,isx,kintbc))/dt
        vbc(:)=vbc(:)+vitrm(:,l)*tmp(:)
      end do
    end do
  end if
  if (isintso4 > 0) then
    do is=1,isintso4
      isx=iinso4(is)
      do l=1,leva
        tmp=(pimast(:,l,isx)*pifrct(:,l,isx,kintso4) &
                -pimas (:,l,isx)*pifrc (:,l,isx,kintso4))/dt
        vsu(:)=vsu(:)+vitrm(:,l)*tmp(:)
      end do
    end do
  end if
  if (isintmd > 0) then
    do is=1,isintmd
      isx=iinmd(is)
      do l=1,leva
        tmp=(pimast(:,l,isx)*pifrct(:,l,isx,kintmd) &
                -pimas (:,l,isx)*pifrc (:,l,isx,kintmd))/dt
        vmd(:)=vmd(:)+vitrm(:,l)*tmp(:)
      end do
    end do
  end if
  if (isintss > 0) then
    do is=1,isintss
      isx=iinss(is)
      do l=1,leva
        tmp=(pimast(:,l,isx)*pifrct(:,l,isx,kintss) &
                -pimas (:,l,isx)*pifrc (:,l,isx,kintss))/dt
        vss(:)=vss(:)+vitrm(:,l)*tmp(:)
      end do
    end do
  end if
  !
  !     * deallocation.
  !
  if (isaext > 0) then
    deallocate(pemast)
  end if
  if (isaint > 0) then
    deallocate(pimast)
    deallocate(pifrct)
  end if
  !
end subroutine diagflx
