!> \file
!> \brief Volume aerosol size distribution using bin scheme - for diagnostic purposes.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine vintsd (vvol,pen0,pephi0,pepsi,pedphi0,pephis0, &
                         pewetrc,penum,pemas,pin0,piphi0,pipsi, &
                         pidphi0,piphis0,piwetrc,pinum,pimas,vitrm, &
                         rads,rade,isdiag,ilga,leva)
  !
  use sdparm, only : aextf,aintf,isaext,isaint,kext,pedryrc,pidryrc,r0
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isdiag !< Number of diagnostic sections
  real, intent(in) :: rads !< Radius lower limit
  real, intent(in) :: rade !< Radius upper limit
  real, intent(in), dimension(ilga,leva) :: vitrm !< Pressure in \f$kg/m^2\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,amplitude) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,width), ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pephis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for externally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaext) :: pedphi0 !< Dry particle radius in the centres of the size
  !< sections for externally mixed aerosol \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: penum !< Aerosol number concentration for externally mixed aerosol \f$[1/kg]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pemas !< Aerosol (dry) mass concentration for externally mixed aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pewetrc !< Total/wet paricle radius \f$[m]\f$, ext. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$, amplitude) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$, width), int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for internally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaint) :: pidphi0 !< Dry particle radius in the centres of the size
  !< sections for internally mixed aerosol \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pinum !< Aerosol number concentration for internally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: piwetrc !< Total/wet paricle radius \f$[m\f$, int. mixture
  real, intent(out), dimension(ilga,isdiag) :: vvol !< Vertically integrated volume size distribution.
  !
  !     internal work variables
  !
  real, dimension(ilga,leva,isdiag) :: fvolo !<
  real, dimension(ilga,leva,isdiag) :: fivol !<
  real, dimension(ilga,leva,isdiag) :: fevol !<
  real, dimension(ilga,leva,isdiag) :: fphis0 !<
  real, dimension(ilga,leva,isdiag) :: fdphi0 !<
  integer :: kx !<
  integer :: is !<
  integer :: il !<
  integer :: l !<
  integer :: nfilt !<
  integer :: nsub !<
  real :: fphis !<
  real :: fphie !<
  real :: fdphi0s !<
  !
  !-----------------------------------------------------------------------
  !     * width of diagnostic sections.
  !
  fphis=log(rads/r0)
  fphie=log(rade/r0)
  fdphi0s=(fphie-fphis)/real(isdiag)
  fdphi0=fdphi0s
  !
  !     * particle sizes corresponding to section boundaries.
  !
  fphis0(:,:,1)=fphis
  do is=2,isdiag
    fphis0(:,:,is)=fphis0(:,:,1)+real(is-1)*fdphi0(:,:,is-1)
  end do
  !
  !-----------------------------------------------------------------------
  !     * strength of filter. no filter, if zero. the particle size
  !     * range of the filter is always smaller than the smallest
  !     * section width.
  !
  if (isaint > 0) then
    nsub=int(aintf%dpstar/fdphi0s)
  end if
  if (isaext > 0) then
    do kx=1,kext
      nsub=min(nsub,int(aextf%tp(kx)%dpstar/fdphi0s))
    end do
  end if
  !      nfilt=int(0.5*(real(nsub)-1.))
  nfilt=1
  !
  !-----------------------------------------------------------------------
  !     * integrate concentrations over sub-sections and filter.
  !
  fvolo=0.
  if (isaint > 0) then
    call intsd(fivol,fphis0,fdphi0,pinum,pimas,pin0,piphi0, &
                   pipsi,piphis0,pidphi0,piwetrc,pidryrc, &
                   isdiag,nfilt,ilga,leva,isaint)
    fvolo=fvolo+fivol
  end if
  if (isaext > 0) then
    call intsd(fevol,fphis0,fdphi0,penum,pemas,pen0,pephi0, &
                   pepsi,pephis0,pedphi0,pewetrc,pedryrc, &
                   isdiag,nfilt,ilga,leva,isaext)
    fvolo=fvolo+fevol
  end if
  !
  !     * vertically integrated volume size distribution.
  !
  do il=1,ilga
    vvol(il,:)=0.
    do l=1,leva
      vvol(il,:)=vvol(il,:)+fvolo(il,l,:)*vitrm(il,l)
    end do
  end do
  !
end subroutine vintsd
