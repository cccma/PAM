!>    \file
!>    \brief Initialization of aerosol size disribution
!!
!!    @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine initphys(xrow,sfrc,iaind,ntracp,ilg,il1,il2,ilev, &
                          ilevp1,msgp1,msgp2,ntrac,imode,its)
  !
  use sdi,    only : bext,bint,cext,cint,ismax,kmax,nmax,sdimod
  use sdparm, only : aextf,imfex,imsex,ina,infex,infin,insex,insin, &
                         isaext,isaint,kext,kint,kpext,kpint, &
                         ntracs,ntract, &
                         pedphis,peismax,peismin,pephiss, &
                         pidphis,piismax,piismin,piphiss, &
                         sextf,sintf,yna,ysmall
  !
  implicit none
  !
  !     * input arrays and scalars
  !
  integer, intent(in), dimension(ntracp) :: iaind !< Aerosol tracer index array
  integer, intent(in) :: ilg !< Horizontal (lat/lon) grid cell index
  integer, intent(in) :: il1 !< Start index of first horizontal grid cell
  integer, intent(in) :: il2 !< Index of last horizontal grid cell
  integer, intent(in) :: ilev !< Number of vertical grid cells
  integer, intent(in) :: ilevp1 !< Number of vertical grid cells
  integer, intent(in) :: imode !< mode number to determine initial aerosol size distribution
  integer, intent(in) :: its !< Number of time steps
  integer, intent(in) :: msgp1 !< Currently unused parameter
  integer, intent(in) :: msgp2 !< Index of highest vertical grid cell
  integer, intent(in) :: ntrac !< Number of tracers
  integer, intent(in) :: ntracp !< Number of aerosol tracers
  !
  !     * output arrays.
  !
  real, intent(out), dimension(ilg,ilevp1,ntrac) :: xrow !< Grid-cell mean tracer mass mixing ratio \f$[kg/kg]\f$
  real, intent(out), dimension(ilg,ilev,ntrac) :: sfrc !< Clear/cloudy concentration difference
  !
  !     * local arrays.
  !
  real, allocatable, dimension(:,:,:) :: trac !<
  real, allocatable, dimension(:,:,:) :: trrat !<
  real, allocatable, dimension(:,:) :: resmt !<
  real, allocatable, dimension(:,:) :: resnt !<
  real, allocatable, dimension(:,:) :: cormt !<
  real, allocatable, dimension(:,:) :: cornt !<
  !
  real, allocatable, dimension(:,:,:) :: pedphi0 !< Dry particle radius in the centres of the size sections for externally mixed aerosol \f$[m]\f$ 
  real, allocatable, dimension(:,:,:) :: pephis0 !< Dry particle size ln(Rp /R0) at the boundaries of the size sections for externally mixed aerosol 
  real, allocatable, dimension(:,:,:) :: peddn !< Density of dry aerosol particle \f$[kg/m^3]\f$, external mixture 
  real, allocatable, dimension(:,:,:) :: pidphi0 !< Dry particle radius in the centres of the size sections for internally mixed aerosol \f$[m]\f$ 
  real, allocatable, dimension(:,:,:) :: piphis0 !< Dry particle size ln(Rp /R0) at the boundaries of the size sections for internally mixed aerosol 
  real, allocatable, dimension(:,:,:) :: piddn !< Density of dry aerosol particle \f$[kg/m^3]\f$, internal mixture 
  real, allocatable, dimension(:,:,:) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,amplitude) ext. mixture
  real, allocatable, dimension(:,:,:) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,width), ext. mixture 
  real, allocatable, dimension(:,:,:) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) ext. mixture
  real, allocatable, dimension(:,:,:) :: penum !< Aerosol number concentration for externally mixed aerosol \f$[1/kg]\f$ 
  real, allocatable, dimension(:,:,:) :: pemas !< Aerosol (dry) mass concentration for externally mixed aerosol \f$[kg/kg]\f$ 
  real, allocatable, dimension(:,:,:) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$, amplitude) int. mixture 
  real, allocatable, dimension(:,:,:) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$, width), int. mixture 
  real, allocatable, dimension(:,:,:) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) int. mixture 
  real, allocatable, dimension(:,:,:) :: pinum !< Aerosol number concentration for internally mixed aerosol \f$[1/kg]\f$ 
  real, allocatable, dimension(:,:,:) :: pimas !< Aerosol (dry) mass concentration for internally mixed aerosol \f$[kg/kg]\f$ 
  real, allocatable, dimension(:,:,:,:) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type 
  integer, allocatable, dimension(:) :: indh !<
  integer, allocatable, dimension(:) :: indv !<
  !
  !     internal work variables
  !
  character(len=2) :: cfnx !<
  character(len=2) :: cfkx !<
  character(len=2) :: cfis !<
  integer :: ilga !< Number of grid points in the horizontal direction
  integer :: leva !< Number of grid points in the vertical direction
  integer :: is !<
  integer :: np !<
  integer :: kx !<
  integer :: ist !<
  integer :: n !<
  integer :: nuout4 !<
  integer :: ns !<
  integer :: nsampl !<
  integer :: nx !<
  integer :: imlin !<
  integer :: ns0 !<
  integer :: ilevi !<
  integer :: ismod !<
  !
  !     * control of pla method. swich for recalculation of variance.
  !
  logical, parameter :: krspsi=.false. !<
  !
  !-----------------------------------------------------------------------
  !     * size of pla arrays.
  !
  leva=ilev
  ilga=il2-il1+1
  !
  !     * allocate memory for work arrays.
  !
  allocate(resmt(ilga,leva))
  allocate(resnt(ilga,leva))
  allocate(cormt(ilga,leva))
  allocate(cornt(ilga,leva))
  allocate(trac (ilga,leva,ntract))
  allocate(trrat(ilga,leva,ntract))
  if (isaext > 0) then
    allocate(pephis0(ilga,leva,isaext))
    allocate(pedphi0(ilga,leva,isaext))
    allocate(peddn  (ilga,leva,isaext))
    allocate(pen0   (ilga,leva,isaext))
    allocate(pephi0 (ilga,leva,isaext))
    allocate(pepsi  (ilga,leva,isaext))
    allocate(penum  (ilga,leva,isaext))
    allocate(pemas  (ilga,leva,isaext))
  end if
  if (isaint > 0) then
    allocate(piphis0(ilga,leva,isaint))
    allocate(pidphi0(ilga,leva,isaint))
    allocate(piddn  (ilga,leva,isaint))
    allocate(pin0   (ilga,leva,isaint))
    allocate(piphi0 (ilga,leva,isaint))
    allocate(pipsi  (ilga,leva,isaint))
    allocate(pinum  (ilga,leva,isaint))
    allocate(pimas  (ilga,leva,isaint))
    allocate(pifrc  (ilga,leva,isaint,kint))
  end if
  !
  !-----------------------------------------------------------------------
  !     aerosol size information.
  !
  do is=1,isaext
    pedphi0(:,:,is)=pedphis(is)
    pephis0(:,:,is)=pephiss(is)
  end do
  do is=1,isaint
    pidphi0(:,:,is)=pidphis(is)
    piphis0(:,:,is)=piphiss(is)
  end do
  !
  !-----------------------------------------------------------------------
  !     input of parameters for initial size distributions
  !     (from input file 'INIT').
  !
  call sdinit(nsampl,ismod)
  ns0=1
  if (imode == 0) then
    ns0=2
  else if (imode == 1) then
    nsampl=1
  else if (imode == 2) then
    continue
  else if (imode == 3) then
    continue
  else if (imode == 4) then
    ns0=1
    nsampl=leva
  else if (imode == 5) then
    ns0=1
    nsampl=leva
  else
    call xit('INITPHYS',-3)
  end if
  !
  !     check if sufficient number of data points are available to
  !     initalize concentrations in model.
  !
  ilevi=nsampl-ns0+1
  if (imode == 0) then
    if (ilevi < leva) call xit('INITPHYS',-1)
  else if (imode == 1) then
    if (leva /= 1) call xit('INITPHYS',-2)
  else if (imode == 3) then
    if (ns0 /= nsampl) call xit('INITPHYS',-3)
  end if
  !
  !-----------------------------------------------------------------------
  !     copy initial size distributions into pla arrays. it is assumed
  !     that the first size disrtibution (index 1 in input file) is
  !     the size distribution at cloud base, which is used if imode=1.
  !     otherwise, imode=0 means that indices > 1 are used to select
  !     the atmospheric profile, and imode=2 means that all indices
  !     are used to initialize the profiles. ns0 is the first index
  !     to be selected and nsampl the number of points in the file.
  !
  allocate(indh(ns0:nsampl))
  allocate(indv(ns0:nsampl))
  indh=1
  indv=ina
  if (imode /= 3) then
    if (imode == 4) then
      do is=1,ismax
        do kx=1,kmax
          cext(2:nmax)%tp(kx)%pn0(is)=cext(1)%tp(kx)%pn0(is)
          cext(2:nmax)%tp(kx)%pphi0(is)=cext(1)%tp(kx)%pphi0(is)
          cext(2:nmax)%tp(kx)%ppsi(is)=cext(1)%tp(kx)%ppsi(is)
          cext(2:nmax)%tp(kx)%num(is)=cext(1)%tp(kx)%num(is)
          cext(2:nmax)%tp(kx)%mas(is)=cext(1)%tp(kx)%mas(is)
          cint(2:nmax)%tp(kx)%conc(is)=cint(1)%tp(kx)%conc(is)
          cint(2:nmax)%tp(kx)%mas(is)=cint(1)%tp(kx)%mas(is)
        end do
        cint(2:nmax)%pn0(is)=cint(1)%pn0(is)
        cint(2:nmax)%pphi0(is)=cint(1)%pphi0(is)
        cint(2:nmax)%ppsi(is)=cint(1)%ppsi(is)
        cint(2:nmax)%num(is)=cint(1)%num(is)
      end do
    end if
    do nx=ns0,nsampl
      indv(nx)=leva-(nx-ns0)
    end do
  end if
  if (kint > 0) then
    call sdcopyi(pinum,pimas,pin0,piphi0,pipsi,pifrc, &
                     indh,indv,nsampl,ns0,ilga,leva)
  end if
  if (kext > 0) then
    call sdcopye(penum,pemas,pen0,pephi0,pepsi, &
                     indh,indv,nsampl,ns0,ilga,leva)
  end if
  deallocate(indh)
  deallocate(indv)
  !
  !     dry particle densities for externally mixed aerosol types.
  !
  do is=1,isaext
    kx=sextf%isaer(is)%ityp
    peddn(:,:,is)=aextf%tp(kx)%dens
  end do
  !
  !     density for internally mixed aerosol.
  !
  if (isaint > 0) call sddens(piddn,pifrc,ilga,leva)
  !
  !     convert input pla parameters to number and mass concentrations
  !     for externally mixed types of aerosols. this ensures that the
  !     pla variance parameter is consistent with the specified
  !     variance. in the following, it is assumed that the input is
  !     for dry air mass mixing ratios.
  !
  if (ismod == 1) then
    if (isaext > 0) &
        call pla2nm(penum,pemas,pen0,pephi0,pepsi,pephis0,pedphi0, &
        peddn,ilga,leva,isaext)
    if (isaint > 0) &
        call pla2nm(pinum,pimas,pin0,piphi0,pipsi,piphis0,pidphi0, &
        piddn,ilga,leva,isaint)
  end if
  if (krspsi .or. ismod==2) then
    !
    !       check aerosol number and mass concentrations and correct,
    !       if necessary.
    !
    if (isaext > 0) then
      call cornmi(resmt,resnt,cormt,cornt,penum,pemas,peddn, &
                      peismin,peismax,pephiss,pedphis,ilga,leva, &
                      isaext)
    end if
    if (isaint > 0) then
      call cornmi(resmt,resnt,cormt,cornt,pinum,pimas,piddn, &
                      piismin,piismax,piphiss,pidphis,ilga,leva, &
                      isaint)
    end if
    !
    !       retrieve basic pla parameters.
    !
    if (isaext > 0) &
        call nm2pla(pen0,pephi0,pepsi,resnt,resmt,penum,pemas, &
        peddn,pephiss,pedphis,pephis0,pedphi0, &
        ilga,leva,isaext,kpext)
    if (isaint > 0) &
        call nm2pla(pin0,piphi0,pipsi,resnt,resmt,pinum,pimas, &
        piddn,piphiss,pidphis,piphis0,pidphi0, &
        ilga,leva,isaint,kpint)
  end if
  !
  !     set concentrations to zero for undefined values.
  !
  if (isaext > 0) then
    where (pen0(:,:,:) <= ysmall)
      penum(:,:,:) = 0.
      pemas(:,:,:) = 0.
    end where
  end if
  if (isaint > 0) then
    where (pin0(:,:,:) <= ysmall)
      pinum(:,:,:) = 0.
      pimas(:,:,:) = 0.
    end where
  end if
  !
  !     write mass and number size distributions for all modes.
  !
  nuout4=33
  open(nuout4,file='OUT4X')
  write(nuout4,'(A5)') ' &INIT'
      write(nuout4,'(1X)')
  write(nuout4,'(A8)') 'SDIMOD=2'
  write(nuout4,'(1X)')
  if (kext > 0) then
    do np=1,leva
      if (np < 10) then
        cfnx='I1'
      else if (np < 100) then
        cfnx='I2'
      else
        cfnx='I3'
      end if
      do kx=1,kext
        do is=1,aextf%tp(kx)%isec
          if (kx < 10) then
            cfkx='I1'
          else if (kx < 100) then
            cfkx='I2'
          else
            cfkx='I3'
          end if
          if (is < 10) then
            cfis='I1'
          else if (is < 100) then
            cfis='I2'
          else
            cfis='I3'
          end if
          write(nuout4, &
               '(A5,'//cfnx//',A5,'//cfkx//',A6,'//cfis//',A2,E15.9)') &
               'BEXT(',np,')%TP(',kx,')%NUM(',is,')=',penum(1,np,is)
          write(nuout4, &
               '(A5,'//cfnx//',A5,'//cfkx//',A6,'//cfis//',A2,E15.9)') &
               'BEXT(',np,')%TP(',kx,')%MAS(',is,')=',pemas(1,np,is)
          write(nuout4,'(1X)')
        end do
      end do
    end do
  end if
  if (kint > 0) then
    do np=1,leva
      if (np < 10) then
        cfnx='I1'
      else if (np < 100) then
        cfnx='I2'
      else
        cfnx='I3'
      end if
      do is=1,isaint
        if (is < 10) then
          cfis='I1'
        else if (is < 100) then
          cfis='I2'
        else
          cfis='I3'
        end if
        write(nuout4, &
               '(A5,'//cfnx//',A6,'//cfis//',A2,E15.9)') &
               'BINT(',np,')%NUM(',is,')=',pinum(1,np,is)
        do kx=1,kint
          if (kx < 10) then
            cfkx='I1'
          else if (kx < 100) then
            cfkx='I2'
          else
            cfkx='I3'
          end if
          write(nuout4, &
               '(A5,'//cfnx//',A5,'//cfkx//',A6,'//cfis//',A2,E15.9)') &
               'BINT(',np,')%TP(',kx,')%MAS(',is,')=', &
                  pimas(1,np,is)*pifrc(1,np,is,kx)
        end do
        write(nuout4,'(1X)')
      end do
    end do
  end if
  write(nuout4,'(A1)') '/'
  close(nuout4)
  !
  !-----------------------------------------------------------------------
  !     copy results into tracer fields.
  !
  if (isaext > 0) then
    trac(:,:,insex:infex)=penum
    trac(:,:,imsex:imfex)=pemas
  end if
  if (isaint > 0) then
    trac(:,:,insin:infin)=pinum
  end if
  do is=1,isaint
    do ist=1,sintf%isaer(is)%itypt
      kx=sintf%isaer(is)%ityp(ist)
      imlin=sintf%isaer(is)%ism(ist)
      trac(:,:,imlin)=pimas(:,:,is)*pifrc(:,:,is,kx)
    end do
  end do
  trrat=yna
  !
  !     pass information to atmospheric model.
  !
  do ns=1,ntracs
    n=iaind(ns)
    xrow(il1:il2,msgp2:ilevp1,n)=trac (1:ilga,1:leva,ns)
    sfrc(il1:il2,msgp1:ilev,n)  =trrat(1:ilga,1:leva,ns)
  end do
  !
  !-----------------------------------------------------------------------
  !     * memory deallocation.
  !
  deallocate(resmt)
  deallocate(resnt)
  deallocate(cormt)
  deallocate(cornt)
  deallocate(trac)
  deallocate(trrat)
  if (isaext > 0) then
    deallocate(pephis0)
    deallocate(pedphi0)
    deallocate(peddn)
    deallocate(pen0)
    deallocate(pephi0)
    deallocate(pepsi)
    deallocate(penum)
    deallocate(pemas)
  end if
  if (isaint > 0) then
    deallocate(piphis0)
    deallocate(pidphi0)
    deallocate(piddn)
    deallocate(pin0)
    deallocate(piphi0)
    deallocate(pipsi)
    deallocate(pinum)
    deallocate(pimas)
    deallocate(pifrc)
  end if
  !
end subroutine initphys
