!> \file
!> \brief Prints out warning message.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine wrn(name,n)
  !
  implicit none
  !
  integer :: i !<
  integer, intent(in) :: n !<
  character(len=*) :: name !<
  character(len=4) :: dash !<
  character(len=4) :: star !<
  character(len=8) :: name8 !<
  character(len=42) :: frmt !<
  !
  FRMT = "FORMAT('0',2A4,'  WARNING  ',A8,17A4,I8)"
  data dash /'----'/, star /'****'/
  !---------------------------------------------------------------------
  !
  name8 = name
  if (n>=0) write(6,frmt) (dash,i=1,2),name8,(dash,i=1,17),n
  !
  if (n<0) write(6,frmt) (star,i=1,2),name8,(star,i=1,17),n
  !
end subroutine wrn
