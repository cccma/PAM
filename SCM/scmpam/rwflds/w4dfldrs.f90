subroutine w4dfldrs(cname,avar,idim1,idim2,idim3,idim4, &
                          j1,j2,k1,k2,l1,l2,m1,m2)
  !
  use iodat
  !
  implicit none
  !
  integer :: ichck
  integer :: it
  !
  character(len=*), intent(in) :: cname !<
  real, intent(in), dimension(idim1,idim2,idim3,idim4) :: avar !<
  integer, intent(in) :: idim1 !<
  integer, intent(in) :: idim2 !<
  integer, intent(in) :: idim3 !<
  integer, intent(in) :: idim4 !<
  integer, intent(in), optional :: j1 !<
  integer, intent(in), optional :: j2 !<
  integer, intent(in), optional :: k1 !<
  integer, intent(in), optional :: k2 !<
  integer, intent(in), optional :: l1 !<
  integer, intent(in), optional :: l2 !<
  integer, intent(in), optional :: m1 !<
  integer, intent(in), optional :: m2 !<
  integer :: j1t !<
  integer :: j2t !<
  integer :: k1t !<
  integer :: k2t !<
  integer :: l1t !<
  integer :: l2t !<
  integer :: m1t !<
  integer :: m2t !<
  integer, dimension(4) :: ishape !<
  !
  if (present(j1) .and. present(j2) &
      .and. present(k1) .and. present(k2) &
      .and. present(l1) .and. present(l2) &
      .and. present(m1) .and. present(m2) ) then
    if (j1 < 1 .or. k1 < 1 .or. l1 < 1 .or. m1 < 1 &
        .or. j2 > idim1 .or. k2 > idim2 .or. l2 > idim3 &
        .or. m2 > idim4) &
        call xit('W4DFLDRS',-1)
    j1t=j1
    j2t=j2
    k1t=k1
    k2t=k2
    l1t=l1
    l2t=l2
    m1t=m1
    m2t=m2
  else
    j1t=1
    j2t=idim1
    k1t=1
    k2t=idim2
    l1t=1
    l2t=idim3
    m1t=1
    m2t=idim4
  end if
  !
  ichck=0
  do it=1,itot
    if (allocated(rvarv(it)%fld4d) &
        .and. trim(rvar(it)%name) == trim(cname) ) then
      ichck=1
      !
      ishape=shape(rvarv(it)%fld4d)
      if (ishape(1) == idim1 .and. ishape(2) == idim2 &
          .and. ishape(3) == idim3 .and. ishape(4) == idim4) then
        rvarv(it)%fld4d(j1t:j2t,k1t:k2t,l1t:l2t,m1t:m2t)= &
                                                              avar(j1t:j2t,k1t:k2t,l1t:l2t,m1t:m2t)
      else
        print*,'INCOMPATIBLE DIMENSIONS VARIABLE ',trim(cname)
        call xit('W4DFLDRS',-2)
      end if
    end if
  end do
  if (ichck==0) then
    print*,'NO VARIABLE ',trim(cname), &
               ' IN LIST OF RESTART VARIABLES'
    call xit('W4DFLDRS',-3)
  end if
  !
end subroutine w4dfldrs
