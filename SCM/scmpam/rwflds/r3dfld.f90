subroutine r3dfld(cname,avar,idim1,idim2,idim3, &
                        j1,j2,k1,k2,l1,l2)
  !
  use iodat
  !
  implicit none
  !
  integer :: i1
  integer :: i2
  integer :: i3
  integer :: ichck
  integer :: it
  !
  character(len=*), intent(in) :: cname !<
  real, intent(out), dimension(idim1,idim2,idim3) :: avar !<
  integer, intent(in), optional :: j1 !<
  integer, intent(in), optional :: j2 !<
  integer, intent(in), optional :: k1 !<
  integer, intent(in), optional :: k2 !<
  integer, intent(in), optional :: l1 !<
  integer, intent(in), optional :: l2 !<
  integer, intent(in) :: idim1 !<
  integer, intent(in) :: idim2 !<
  integer, intent(in) :: idim3 !<
  integer :: j1t !<
  integer :: j2t !<
  integer :: k1t !<
  integer :: k2t !<
  integer :: l1t !<
  integer :: l2t !<
  integer, dimension(3) :: ishape !<
  !
  if (present(j1) .and. present(j2) &
      .and. present(k1) .and. present(k2) &
      .and. present(l1) .and. present(l2) ) then
    if (j1 < 1 .or. k1 < 1 .or. l1 < 1 &
        .or. j2 > idim1 .or. k2 > idim2 .or. l2 > idim3) &
        call xit('R3DFLD',-1)
    j1t=j1
    j2t=j2
    k1t=k1
    k2t=k2
    l1t=l1
    l2t=l2
  else
    j1t=1
    j2t=idim1
    k1t=1
    k2t=idim2
    l1t=1
    l2t=idim3
  end if
  !
  ichck=0
  do it=1,itot
    if (allocated(ivarv(it)%fld3d) &
        .and. trim(ivar(it)%name) == trim(cname) ) then
      ichck=1
      !
      ishape=shape(ivarv(it)%fld3d)
      if (ishape(1) == idim1 .and. ishape(2) == idim2 &
          .and. ishape(3) == idim3) then
        avar(j1t:j2t,k1t:k2t,l1t:l2t)= &
                                           ivarv(it)%fld3d(j1t:j2t,k1t:k2t,l1t:l2t)
      else if (ishape(1) == idim1 .and. ishape(2) == idim3 &
               .and. ishape(3) == idim2) then
        do i2=k1t,k2t
          do i3=l1t,l2t
            avar(j1t:j2t,i2,i3)=ivarv(it)%fld3d(j1t:j2t,i3,i2)
          end do
        end do
      else if (ishape(1) == idim2 .and. ishape(2) == idim1 &
               .and. ishape(3) == idim3) then
        do i1=j1t,j2t
          do i2=k1t,k2t
            avar(i1,i2,l1t:l2t)=ivarv(it)%fld3d(i2,i1,l1t:l2t)
          end do
        end do
      else if (ishape(1) == idim2 .and. ishape(2) == idim3 &
               .and. ishape(3) == idim1) then
        do i1=j1t,j2t
          do i2=k1t,k2t
            do i3=l1t,l2t
              avar(i1,i2,i3)=ivarv(it)%fld3d(i2,i3,i1)
            end do
          end do
        end do
      else if (ishape(1) == idim3 .and. ishape(2) == idim1 &
               .and. ishape(3) == idim2) then
        do i1=j1t,j2t
          do i2=k1t,k2t
            do i3=l1t,l2t
              avar(i1,i2,i3)=ivarv(it)%fld3d(i3,i1,i2)
            end do
          end do
        end do
      else if (ishape(1) == idim3 .and. ishape(2) == idim2 &
               .and. ishape(3) == idim1) then
        do i1=j1t,j2t
          do i3=l1t,l2t
            avar(i1,k1t:k2t,i3)=ivarv(it)%fld3d(i3,k1t:k2t,i1)
          end do
        end do
      else
        print*,'INCOMPATIBLE DIMENSIONS VARIABLE ',trim(cname)
        call xit('R3DFLD',-2)
      end if
    end if
  end do
  if (ichck==0) then
    print*,'NO VARIABLE ',trim(cname),' IN LIST OF IO VARIABLES'
    call xit('R3DFLD',-3)
  end if
  !
  !      print '("R3DFLD: ", a10, "   [",i2,"x",i2,"x",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]", es12.2)', &
  !       cname,idim1,idim2,idim3,j1t,j2t,k1t,k2t,l1t,l2t, sum(avar)/size(avar)
end subroutine r3dfld
