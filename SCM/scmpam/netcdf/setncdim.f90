subroutine setncdim(nstpo,idim)
  !
  use iodat
  !
  implicit none
  !
  integer, intent(in) :: idim
  integer, intent(in) :: nstpo
  !
  !     * define netcdf dimensions and associate with dimensions
  !     * in the model.
  !
  print*,'SETTING DIMENSION ',trim(dim(idim)%name)
  dim(idim)%lname(1:12)='Output time '
  dim(idim)%units(1:12)='s           '
  dim(idim)%size=nstpo
  allocate(dim(idim)%val(dim(idim)%size))
  !
end subroutine setncdim
